package com.example.unit_test.ticTacToe;

import com.example.unit_test.domain.Step;
import com.example.unit_test.repo.StepRepository;
import com.example.unit_test.ticTacToe.impl.TicTacToeImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class TicTacToeSpec {

    @InjectMocks
    public TicTacToeImpl ticTacToe;
    @Mock
    private StepRepository repository;

//    @BeforeEach
//    public void before() {
//        ticTacToe = new TicTacToe(repository);
//    }

    @Test
    public void whenXOutsideBoardThenRuntimeException() {
        Assertions.assertThatThrownBy(() -> ticTacToe.play(5, 2))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    public void whenYOutsideBoardThenRuntimeException() {
        Assertions.assertThatThrownBy(() -> ticTacToe.play(2, 5))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("O is outside board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        // arrange
        Step step = new Step();
        step.setPlayer('X');
        step.setXCord(1);
        step.setYCord(1);

        List<Step> steps = List.of(step);

        Mockito.when(repository.findAll()).thenReturn(steps);

        // act & assert
        Assertions.assertThatThrownBy(() -> ticTacToe.play(1, 1)) // player O
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        Assertions.assertThat(ticTacToe.nextPlayer())
                .isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO() {
        // arrange
        Step step = new Step();
        step.setXCord(1);
        step.setYCord(1);
        step.setPlayer('X');
        List<Step> steps = List.of(step);
        Mockito.when(repository.findAll()).thenReturn(steps);

        // act & assert
        Assertions.assertThat(ticTacToe.nextPlayer())
                .isEqualTo('O');
    }

    @Test
    public void whenPlayThenNoWinner() {
        // arrange
        Step step = new Step();
        step.setXCord(1);
        step.setYCord(1);
        step.setPlayer('X');
        List<Step> steps = List.of(step);
        Mockito.when(repository.findAll()).thenReturn(steps);

        String actual = ticTacToe.play(1, 2);
        Assertions.assertThat(actual).isEqualTo("No winner");
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        // arrange
        Step x1 = new Step(1L, 1, 1, 'X');
        Step o1 = new Step(2L, 1, 2, 'Y');
        Step x2 = new Step(3L, 2, 1, 'X');
        Step o2 = new Step(4L, 2, 2, 'Y');
        List<Step> steps = List.of(x1, x2, o1, o2);

        Mockito.when(repository.findAll()).thenReturn(steps);

        // act & assert
        Assertions.assertThat(ticTacToe.play(3, 1))
                .isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        // arrange
        Step x1 = new Step(1L, 2, 1, 'X');
        Step o1 = new Step(2L, 1, 1, 'O');
        Step x2 = new Step(3L, 3, 1, 'X');
        Step o2 = new Step(4L, 1, 2, 'O');
        Step x3 = new Step(5L, 2, 2, 'X');
        List<Step> steps = List.of(x1, o1, x2, o2, x3);
        Mockito.when(repository.findAll()).thenReturn(steps);

        // act & arrange
        Assertions.assertThat(ticTacToe.play(1, 3))
                .isEqualTo("O is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        // arrange
        Step x1 = new Step(1L, 1, 1, 'X');
        Step o1 = new Step(2L, 1, 2, 'O');
        Step x2 = new Step(3L, 2, 2, 'X');
        Step o2 = new Step(4L, 1, 3, 'O');
        List<Step> steps = List.of(x1, o1, x2, o2);
        Mockito.when(repository.findAll()).thenReturn(steps);

        // act & assert
        Assertions.assertThat(ticTacToe.play(3, 3))
                .isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        // arrange
        Step x1 = new Step(1L, 1, 3, 'X');
        Step o1 = new Step(2L, 1, 1, 'O');
        Step x2 = new Step(3L, 2, 2, 'X');
        Step o2 = new Step(4L, 1, 2, 'O');
        List<Step> steps = List.of(x1, o1, x2, o2);
        Mockito.when(repository.findAll()).thenReturn(steps);

        // act & assert
        Assertions.assertThat(ticTacToe.play(3, 1))
                .isEqualTo("X is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        // arrange
        Step x1 = new Step(1L, 1, 1, 'X');
        Step o1 = new Step(2L, 1, 3, 'O');
        Step x2 = new Step(3L, 1, 2, 'X');
        Step o2 = new Step(4L, 2, 1, 'O');
        Step x3 = new Step(5L, 2, 3, 'X');
        Step o3 = new Step(6L, 2, 2, 'O');
        Step x4 = new Step(7L, 3, 2, 'X');
        Step o4 = new Step(8L, 3, 1, 'O');
        List<Step> steps = List.of(x1, o1, x2, o2, x3, o3, x4, o4);
        Mockito.when(repository.findAll()).thenReturn(steps);

        // act & assert
        Assertions.assertThat(ticTacToe.play(3, 3))
                .isEqualTo("The result is draw");
    }
}
