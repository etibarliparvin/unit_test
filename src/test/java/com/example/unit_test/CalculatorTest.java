package com.example.unit_test;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {

    @InjectMocks
    private CalculatorImpl calculator;

    @Test
    public void givenAAndBWhenAddThenC() {
        // arrange
        int a = 5;
        int b = 2;
        int c = 7;

        // act
        int result = calculator.add(a, b);

        // assert
        Assertions.assertThat(result).isEqualTo(c);
    }

    @Test
    public void givenAAndBWhenDivideThenC() {
        // arrange
        int a = 14;
        int b = 2;
        int c = 7;

        // act
        int result = calculator.divide(a, b);

        // assert
        Assertions.assertThat(result).isEqualTo(c);
    }

    @Test
    public void givenAAndBWhenDivideByZeroThenException() {
        // arrange
        int a = 14;
        int b = 0;

        // act & assert
        Assertions.assertThatThrownBy(() -> calculator.divide(a, b))
                .isInstanceOf(ArithmeticException.class)
                .hasMessage("/ by zero");
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    public void givenValueWhenMultiplyToTwoThenSuccess(int value) {
        // arrange
        int result = value * 2;

        // act
        int actual = calculator.multiply(value, 2);

        // assert
        Assertions.assertThat(result).isEqualTo(actual);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    public void givenValueWhenAddThenSuccess(int a, int b) {
        // arrange
        int result = a + b;

        // act
        int actual = calculator.add(a, b);

        // assert
        Assertions.assertThat(result).isEqualTo(actual);
    }
}
