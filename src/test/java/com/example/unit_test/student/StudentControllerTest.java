package com.example.unit_test.student;

import com.example.unit_test.controller.StudentController;
import com.example.unit_test.dto.response.StudentResponse;
import com.example.unit_test.service.StudentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StudentController.class)
@ExtendWith(MockitoExtension.class)
public class StudentControllerTest {

    @Mock
    private StudentService service;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenGetAllStudentsThenSuccess() throws Exception {
        // arrange
        StudentResponse response1 = new StudentResponse();
        response1.setName("Parvin");
        response1.setAge(32);
        Mockito.when(service.findAll()).thenReturn(List.of(response1));

        // act
        mockMvc.perform(MockMvcRequestBuilders.get("/student"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(""));
    }
}
