package com.example.unit_test.student;

import com.example.unit_test.domain.Student;
import com.example.unit_test.dto.request.StudentCreateDto;
import com.example.unit_test.dto.response.StudentResponse;
import com.example.unit_test.mapper.StudentMapper;
import com.example.unit_test.repository.StudentRepository;
import com.example.unit_test.service.impl.StudentServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

/* 100% line coverage does not mean 100% test coverage
but 100% test coverage means 100% line coverage
*/
@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {

    @InjectMocks
    private StudentServiceImpl service;
    @Mock
    private StudentRepository repository;
    @Mock
    private StudentMapper mapper;
    @Captor
    private ArgumentCaptor<Student> studentArgumentCaptor;

    @Test
    public void whenGetStudentThenStudentDto() {
        // arrange
        Student student = new Student();
        student.setId(1L);
        student.setName("Parvin");
        student.setAge(32);

        StudentResponse studentResponse = new StudentResponse();
        studentResponse.setName("Parvin");
        studentResponse.setAge(32);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(student));
        Mockito.when(mapper.toResponse(student)).thenReturn(studentResponse);

        // act
        StudentResponse response = service.findById(1L);

        // assert
        Assertions.assertThat(response.getName()).isEqualTo(student.getName());
        Assertions.assertThat(response).isEqualTo(studentResponse);
    }

    @Test
    public void whenGetStudentWithAgeLessThan18ThenThrowException() {
        // arrange
        Student student = new Student();
        student.setId(1L);
        student.setName("Parvin");
        student.setAge(6);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(student));

        // act & assert
        Assertions.assertThatThrownBy(() -> service.findById(1L))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("You don't pass age requirements");
    }

    @Test
    public void whenGetStudentWithAgeLessThan18ThenThrowException2() {
        // arrange
        Mockito.when(repository.findById(anyLong())).thenReturn(Optional.empty());

        // act & assert
        Assertions.assertThatThrownBy(() -> service.findById(anyLong()))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Student not found");
    }

    @Test
    public void whenNoStudentThenThrowStudentNotFound() {
        // act & assert
        Assertions.assertThatThrownBy(() -> service.findById(1L))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Student not found");
    }

    @Test
    public void whenCreateStudentThenNameUpperCase() {
        // arrange
        StudentCreateDto dto = new StudentCreateDto();
        dto.setName("parvin");
        dto.setAge(32);

        Student student = new Student();
//        student.setId(1L);
        student.setName("PARVIN");
        student.setAge(32);

        StudentResponse response = new StudentResponse();
        response.setName("PARVIN");
        response.setAge(32);

        Mockito.when(repository.save(student)).thenReturn(student);
        Mockito.when(mapper.toEntity(dto)).thenReturn(student);
        Mockito.when(mapper.toResponse(student)).thenReturn(response);

        // act
        StudentResponse save = service.save(dto);

        // assert
        Mockito.verify(repository, Mockito.times(1)).save(student);
//        Mockito.verify(mapper, Mockito.times(1)).toResponse(student);
//        Mockito.verify(mapper, Mockito.times(1)).toEntity(dto);
        Assertions.assertThat(save).isEqualTo(response);
        Assertions.assertThat(student.getName()).isEqualTo(response.getName());
    }

    @Test
    public void whenCreateStudentWithDateCaptor() throws ParseException {
        // arrange
        StudentCreateDto dto = new StudentCreateDto();
        dto.setName("parvin");
        dto.setAge(32);

        Student student = new Student();
        student.setName("PARVIN");
        student.setAge(32);
        String date = "31-Dec-1998 23:37:50";
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        Date parse = format.parse(date);
        student.setCreationDate(parse);

        Mockito.when(mapper.toEntity(dto)).thenReturn(student);
        Mockito.when(repository.save(any())).thenReturn(student);

        // act
        service.save(dto);

        // assert
        Mockito.verify(repository, Mockito.times(1)).save(studentArgumentCaptor.capture());
        Student student1 = studentArgumentCaptor.getValue();
        Assertions.assertThat(student1.getCreationDate()).isEqualTo(student.getCreationDate());
    }
}
