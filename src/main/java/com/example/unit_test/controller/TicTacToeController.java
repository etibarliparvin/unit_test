package com.example.unit_test.controller;

import com.example.unit_test.dto.request.MoveRequest;
import com.example.unit_test.dto.response.Result;
import com.example.unit_test.ticTacToe.TicTacToe;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TicTacToeController {

    private final TicTacToe ticTacToe;

    @PostMapping
    public ResponseEntity<Result> play(@RequestBody MoveRequest request) {
        String play = ticTacToe.play(request.getX(), request.getY());
        return ResponseEntity.ok(new Result(play));
    }
}
