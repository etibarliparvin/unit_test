package com.example.unit_test.mapper;

import com.example.unit_test.domain.Student;
import com.example.unit_test.dto.request.StudentCreateDto;
import com.example.unit_test.dto.response.StudentResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {

    Student toEntity(StudentCreateDto dto);

    StudentResponse toResponse(Student student);

    List<StudentResponse> toResponseList(List<Student> students);
}
