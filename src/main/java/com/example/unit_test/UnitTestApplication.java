package com.example.unit_test;

import com.example.unit_test.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class UnitTestApplication {

    private final StudentService service;

    public static void main(String[] args) {
        SpringApplication.run(UnitTestApplication.class, args);
    }

}
