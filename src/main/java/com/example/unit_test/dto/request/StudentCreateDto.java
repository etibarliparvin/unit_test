package com.example.unit_test.dto.request;

import lombok.Data;

@Data
public class StudentCreateDto {
    private String name;
    private Integer age;
}
