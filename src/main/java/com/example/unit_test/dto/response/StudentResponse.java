package com.example.unit_test.dto.response;

import lombok.Data;

@Data
public class StudentResponse {
    private String name;
    private Integer age;
}
