package com.example.unit_test.ticTacToe.impl;

import com.example.unit_test.domain.Step;
import com.example.unit_test.repo.StepRepository;
import com.example.unit_test.ticTacToe.TicTacToe;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TicTacToeImpl implements TicTacToe {

    private final StepRepository stepRepository;

    private static final int SIZE = 3;

    @Override
    public String play(int x, int y) {
        checkAxis('X', x);
        checkAxis('O', y);
        Character[][] board = loadSteps();
        char lastPlayer = nextPlayer();
        setBox(board, x, y, lastPlayer);
        if (isWin(board, x, y, lastPlayer)) {
            return lastPlayer + " is the winner";
        } else if (isDraw(board)) {
            return "The result is draw";
        }
        return "No winner";
    }

    private boolean isWin(Character[][] board, int x, int y, char lastPlayer) {
        int playerTotal = lastPlayer * 3;
        char horizontal = '\0', vertical = '\0', diagonal1 = '\0', diagonal2 = '\0';
        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][y - 1];
            vertical += board[x - 1][i];
            diagonal1 += board[i][i];
            diagonal2 += board[i][SIZE - i - 1];
        }
        return horizontal == playerTotal || vertical == playerTotal
                || diagonal1 == playerTotal || diagonal2 == playerTotal;
    }

    private boolean isDraw(Character[][] board) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (board[i][j] == '\0')
                    return false;
            }
        }
        return true;
    }

    private void checkAxis(char player, int axis) {
        if (axis < 1 || axis > 3) {
            throw new RuntimeException(player + " is outside board");
        }
    }

    private void setBox(Character[][] board, int x, int y, char lastPlayer) {
        if (board[x - 1][y - 1] != '\0') {
            throw new RuntimeException("Box is occupied");
        } else {
            board[x - 1][y - 1] = lastPlayer;
            saveMove(x, y, lastPlayer);
        }
    }

    public char nextPlayer() {
        List<Step> steps = stepRepository.findAll();
        if (steps.size() == 0)
            return 'X';
        char player = steps.get(steps.size() - 1).getPlayer();
        return player == 'X' ? 'O' : 'X';
    }

    public void saveMove(int x, int y, char player) {
        Step step = new Step();
        step.setXCord(x);
        step.setYCord(y);
        step.setPlayer(player);
        stepRepository.save(step);
    }

    private Character[][] loadSteps() {
        Character[][] board = {
                {'\0', '\0', '\0'},
                {'\0', '\0', '\0'},
                {'\0', '\0', '\0'}};
        stepRepository.findAll()
                .forEach(step -> board[step.getXCord() - 1][step.getYCord() - 1] = step.getPlayer());
        printBoard(board);
        return board;
    }

    private void printBoard(Character[][] board) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }
}
