package com.example.unit_test.ticTacToe;

public interface TicTacToe {
    String play(int x, int y);
}
