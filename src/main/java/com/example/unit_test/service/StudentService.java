package com.example.unit_test.service;

import com.example.unit_test.dto.request.StudentCreateDto;
import com.example.unit_test.dto.response.StudentResponse;

import java.util.List;

public interface StudentService {

    StudentResponse findById(Long id);

    List<StudentResponse> findAll();

    StudentResponse save(StudentCreateDto dto);
}
