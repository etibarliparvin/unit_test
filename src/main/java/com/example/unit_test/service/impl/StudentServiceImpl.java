package com.example.unit_test.service.impl;

import com.example.unit_test.domain.Student;
import com.example.unit_test.dto.request.StudentCreateDto;
import com.example.unit_test.dto.response.StudentResponse;
import com.example.unit_test.mapper.StudentMapper;
import com.example.unit_test.repository.StudentRepository;
import com.example.unit_test.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;
    private final StudentMapper mapper;

    @Override
    public StudentResponse findById(Long id) {
        Student student = repository.findById(id).orElseThrow(() -> new RuntimeException("Student not found"));
        if (student.getAge() > 18)
            return mapper.toResponse(student);
        else throw new RuntimeException("You don't pass age requirements");
    }

    @Override
    public List<StudentResponse> findAll() {
        return mapper.toResponseList(repository.findAll());
    }

    @Override
    public StudentResponse save(StudentCreateDto dto) {
        dto.setName(dto.getName().toUpperCase());
        Student student = mapper.toEntity(dto);
        student.setCreationDate(new Date());
        Student save = repository.save(student);
        return mapper.toResponse(save);
    }
}
